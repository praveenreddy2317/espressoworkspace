Feature: Test text
  SameActivity Test

  @smoke
  Scenario Outline: Test text on Same activity
    Given user started the app
    When user input text "<msg>"
    And user press submit button
    Then user should see "<expMsg>" on same activity
    Examples:
      | msg      |expMsg|
      | praveen |praveen|
      | kumar    |kumar   |

  @wip
  Scenario Outline: Work in Progress
    Given user started the app
    When user input text "<msg>"
    And user press submit button
    Then user should see "<expMsg>" on same activity
    Examples:
      | msg      |expMsg|
      | prav |prav|
      | kumar    |kumar   |



