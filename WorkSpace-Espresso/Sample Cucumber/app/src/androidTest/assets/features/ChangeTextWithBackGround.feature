Feature: Test text with Back ground feature
  SameActivity Test with BG feature

  Background: User is Logged In
    Given user started the app
  @smoke
  Scenario Outline: Test text on Same activity
    Given user input text "<msg>"
    When user press submit button
    Then user should see "<expMsg>" on same activity
    Examples:
      | msg      |expMsg|
      | praveen |praveen|
      | kumar2317    |kumar2317   |

  @smoke
  Scenario: Work in Progress
    Given user input text "pr2317"
    And user press submit button
    Then user should see "pr2317" on same activity



