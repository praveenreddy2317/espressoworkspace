Feature: Test text Without Example
  SameActivity Test

  @smoke
  Scenario: Test text on Same activity
    Given user started the app
    When user input text "praveen"
    And user press submit button
    Then user should see "praveen" on same activity


  @wip
  Scenario: Work in Progress-Won't execute
    Given user started the app
    When user input text "kumar"
    And user press submit button
    Then user should see "kumar" on same activity




