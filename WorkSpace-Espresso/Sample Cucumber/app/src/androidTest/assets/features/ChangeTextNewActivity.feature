Feature: Test text on New Activity
  NewActivity Test

  @smoke
  Scenario Outline: Test text on New activity
    Given user started the app
    When user input text "<msg>"
    And user press on open new activity button
    Then user should see "<expMsg>" on new activity
    Examples:
      | msg      |expMsg|
      | newActivity |newActivity|
      | text on NewActivity    |text on NewActivity   |



