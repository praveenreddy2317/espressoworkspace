package com.example.bdd.test

import cucumber.api.CucumberOptions
import cucumber.api.android.CucumberAndroidJUnitRunner


@CucumberOptions(glue = ["com.example.bdd.steps"], features = ["features"], tags = ["@smoke","~@wip"])

//@SuppressWarnings("unused")
class CucumberTestRunner : CucumberAndroidJUnitRunner()