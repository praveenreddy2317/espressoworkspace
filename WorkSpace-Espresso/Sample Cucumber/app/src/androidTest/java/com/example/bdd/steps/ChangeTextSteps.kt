package com.example.bdd.steps

import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.android.testing.espresso.BasicSample.MainActivity
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.matcher.ViewMatchers
import com.example.android.testing.espresso.BasicSample.R
import cucumber.api.java.After
import cucumber.api.java.Before

//STEP Definitions
@RunWith(AndroidJUnit4::class)

class ChangeTextSteps {

    @get:Rule
    var activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    @Before
    fun setup() {
        Intents.init()
    }

    @After
    fun cleanup() {
        Intents.release()
    }


    @Given("user started the app")
    fun the_app_started() {
        activityRule.launchActivity(Intent())

    }

    @And("user press on open new activity button")
    fun clickOnNewActivityButton() {
        Espresso.onView(ViewMatchers.withId(R.id.activityChangeTextBtn)).perform(ViewActions.click())

    }

    @Then("user should see {string} on new activity")
    fun verifyTextOnNewActivity(msg: String) {
        Espresso.onView(ViewMatchers.withId(R.id.show_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(msg)))

    }
    @When("user input text {string}")
    fun enterText(msg: String) {
        Espresso.onView(ViewMatchers.withId(R.id.editTextUserInput)).perform(ViewActions.clearText())
        Espresso.onView(ViewMatchers.withId(R.id.editTextUserInput)).perform(ViewActions.typeText(msg), ViewActions.closeSoftKeyboard())


    }
    @And("user press submit button")
    fun clickOnButton() {
        Espresso.onView(ViewMatchers.withId(R.id.changeTextBt)).perform(ViewActions.click())

    }

    @Then("user should see {string} on same activity")
    fun verifyText(msg: String) {
        Espresso.onView(ViewMatchers.withId(R.id.textToBeChanged)).check(ViewAssertions.matches(ViewMatchers.withText(msg)))

    }



}