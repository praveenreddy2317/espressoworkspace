package com.ui

import com.example.android.testing.espresso.DataAdapterSample.R

class LongListActivityScreen : CommonUtils<LongListActivityScreen>() {

    fun clickOnListItem(text: String): LongListActivityScreen {

        return clickItemOnListViewByText(text, "ROW_TEXT")

    }

    fun clickOnListItemAtPosition(itemPosition: Int): LongListActivityScreen {

        return clickItemInAdapterView(PARENT_LIST_VIEW, itemPosition)

    }

    fun clickOnToggleButtonAtPosition(itemPosition: Int): LongListActivityScreen {

        return clickItemInAdapterView(PARENT_LIST_VIEW, CHILD_LIST_TOGGLEBUTTON, itemPosition)

    }


    fun checkListItemAtPosition(itemPosition: Int, expText: String): LongListActivityScreen {
        return checkItemInAdapterView(PARENT_LIST_VIEW, CHILD_LIST_ITEMTEXT, itemPosition, expText)

    }


    fun checkToggleButtonONOFF(itemPosition: Int, expText: String): LongListActivityScreen {
        return checkItemInAdapterView(PARENT_LIST_VIEW, CHILD_LIST_TOGGLEBUTTON, itemPosition, expText)

    }

    fun checkListSize(size: Int): LongListActivityScreen {
        return checkItemsInAdapterView(PARENT_LIST_VIEW, CHILD_LIST_ITEMTEXT, size)

    }

    fun verifySelectedItemOnListView(text: String): LongListActivityScreen {
        return verifyTextOnView(SELECTED_ITEM_TEXT, text)

    }

    companion object {

        val PARENT_LIST_VIEW = R.id.list
        val SELECTED_ITEM_TEXT = R.id.selection_row_value
        val CHILD_LIST_ITEMTEXT = R.id.rowContentTextView
        val CHILD_LIST_TOGGLEBUTTON = R.id.rowToggleButton


    }

}