package com.ui

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.matchers.CustomMatcher.Companion.withListSize
import org.hamcrest.Matchers.*

@Suppress("UNCHECKED_CAST")
abstract class CommonUtils<T : CommonUtils<T>> {

    fun clickOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())
        return this as T

    }

    fun enterTextIntoView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())
        return this as T
    }

    fun clearTextOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.clearText())
        return this as T
    }

    fun verifyTextOnView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).check(ViewAssertions.matches(ViewMatchers.withText(text)))

        return this as T
    }

    //###################### $ADAPTER VIEW$ ##############################

    fun clickItemOnListViewByText(itemName: String, ROW_TEXT: String): T {

        //DataInteraction
        onData(allOf(`is`(instanceOf(Map::class.java)), hasEntry(equalTo(ROW_TEXT),
                `is`(itemName)))).perform(click())


        return this as T
    }

    fun checkItemOnListView(itemNameOnView: String, expItemName: String): T {

        onData(allOf(`is`(instanceOf(String::class.java)), `is`(itemNameOnView))).check(matches(withText(expItemName)))

        return this as T
    }

    fun clickItemInAdapterView(@IdRes viewId: Int, itemPosition: Int): T {

        onData(allOf())
                .inAdapterView(withId(viewId))
                .atPosition(itemPosition)
                .perform(click())
        return this as T
    }

    fun clickItemInAdapterView(@IdRes viewId: Int, @IdRes childViewId: Int, itemPosition: Int): T {

        onData(allOf())
                .inAdapterView(withId(viewId))
                .atPosition(itemPosition)
                .onChildView(withId(childViewId))
                .perform(click())
        return this as T
    }

    fun checkItemInAdapterView(@IdRes viewId: Int, @IdRes childVewId: Int, itemPosition: Int, expItemName: String): T {

        onData(allOf())
                .inAdapterView(withId(viewId))
                .atPosition(itemPosition)
                .onChildView(withId(childVewId))
                .check(matches(withText(expItemName)))
        return this as T
    }

    fun checkItemsInAdapterView(@IdRes viewId: Int, @IdRes childVewId: Int, size: Int): T {

        onView(withId(viewId)).check(matches(withListSize(size)))
        return this as T
    }


    //###################### $ADAPTER VIEW$ ##############################

    companion object {
        fun <T : CommonUtils<*>> useFactory(screenUtilsClass: Class<T>?): T {
            if (screenUtilsClass == null)
                throw IllegalArgumentException("Pointing to NULL Reference")
            try {
                return screenUtilsClass.newInstance()
            } catch (iae: IllegalStateException) {
                throw RuntimeException("IllegalStateException", iae)
            }
        }
    }
}