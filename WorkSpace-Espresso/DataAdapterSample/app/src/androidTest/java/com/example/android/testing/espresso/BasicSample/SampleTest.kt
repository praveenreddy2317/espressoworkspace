package com.example.android.testing.espresso.BasicSample

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.android.testing.espresso.DataAdapterSample.LongListActivity
import com.ui.LongListActivityScreen
import com.ui.CommonUtils.Companion.useFactory
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest

class SampleTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(LongListActivity::class.java)


    @Test
    fun testSelectedItemByPosition() {

        useFactory(LongListActivityScreen::class.java).clickOnListItemAtPosition(ITEM_POSITION)
                .verifySelectedItemOnListView(EXPECTED_TEXT)

    }

    @Test
    fun testSelectedItemByText() {

        useFactory(LongListActivityScreen::class.java).clickOnListItem(ITEM79_TEXT)
                .verifySelectedItemOnListView(EXPECTED79_TEXT)

    }

    @Test
    fun testItemAtPosition() {

        useFactory(LongListActivityScreen::class.java).checkListItemAtPosition(ITEM_POSITION, EXPECTED_ITEM_TEXT)

    }

    //I have used Custom Matcher to match No of items in ListView
    @Test
    fun testItems() {
        useFactory(LongListActivityScreen::class.java).checkListSize(NUMBER_OF_ITEMS)
    }

    @Test

    fun testToggleButton() {

        useFactory(LongListActivityScreen::class.java).clickOnToggleButtonAtPosition(ITEM_POSITION)
                .checkToggleButtonONOFF(ITEM_POSITION, "ON")
                .clickOnToggleButtonAtPosition(ITEM_POSITION)
                .checkToggleButtonONOFF(ITEM_POSITION, "OFF")

    }


    companion object {

        val ITEM79_TEXT = "item: 79"
        val EXPECTED79_TEXT = "79"
        val ITEM_POSITION = 89
        val EXPECTED_TEXT = "89"
        val EXPECTED_ITEM_TEXT = "item: 89"
        val NUMBER_OF_ITEMS = 100

    }
}