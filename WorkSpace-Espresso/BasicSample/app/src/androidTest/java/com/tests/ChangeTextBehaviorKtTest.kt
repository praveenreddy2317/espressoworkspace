package com.tests

import android.app.Activity
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.ui.MainActivityScreen
import com.ui.CommonUtils
import com.ui.CommonUtils.Companion.useFactory
import com.ui.ShowTextActivityScreen
import com.example.android.testing.espresso.BasicSample.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

//My Test class
@RunWith(AndroidJUnit4::class)
@LargeTest
class ChangeTextBehaviorKtTest {

    //Junit rules work based on AOP principles .
    // So this rule launch MainActivity before running test and terminate after execution of  test method
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    //Test methods
    @Test
    fun testChangeTextOnSameActivity() {

        useFactory(MainActivityScreen::class.java).changeTextOnSameActivity(STRING_TO_BE_TYPED)
                .verifyTextOnMainActivity(EXPECTED_TEXT)

    }

    @Test
    fun testChangeTextNewActivity() {

        CommonUtils.useFactory(MainActivityScreen::class.java)
                .changeTextOnNewActivity(STRING_TO_BE_TYPED)
        CommonUtils.useFactory(ShowTextActivityScreen::class.java)
                .verifyTextOnShowActivity(EXPECTED_TEXT)


    }


    companion object {
        //My Test Data

        val STRING_TO_BE_TYPED = "Praveen Kumar"
        val EXPECTED_TEXT = "Praveen Kumar"

    }
}