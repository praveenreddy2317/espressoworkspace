package com.ui

import com.example.android.testing.espresso.BasicSample.R

//I have created one class per  Activity screen
//Implemented Page Object Model
class MainActivityScreen : CommonUtils<MainActivityScreen>() {

    //Reusable methods
    fun changeTextOnSameActivity(text: String): MainActivityScreen {

        return clearTextOnView(TEXT_INPUT)
                .enterTextIntoView(TEXT_INPUT, text)
                .clickOnView(CHANGETEXT_BUTTON)
                .verifyTextOnView(EXPTEXT_LABEL, text)

    }

    fun changeTextOnNewActivity(text: String): MainActivityScreen {
        return clearTextOnView(TEXT_INPUT)
                .enterTextIntoView(TEXT_INPUT, text)
                .clickOnView(ACTIVITYCHANGETEXT_BUTTON)

    }

    fun verifyTextOnMainActivity(text: String): MainActivityScreen {
        return verifyTextOnView(EXPTEXT_LABEL, text)

    }

    companion object {
        //Locators
        val TEXT_INPUT = R.id.editTextUserInput
        val CHANGETEXT_BUTTON = R.id.changeTextBt
        val EXPTEXT_LABEL = R.id.textToBeChanged
        val ACTIVITYCHANGETEXT_BUTTON = R.id.activityChangeTextBtn
    }
}

