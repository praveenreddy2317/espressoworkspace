package com.ui

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers

//Espresso basic ViewMatchers,ViewActions & ViewAssertions
@Suppress("UNCHECKED_CAST")
abstract class CommonUtils<T : CommonUtils<T>> {

    fun clickOnView(@IdRes viewId: Int): T {
        //ViewInteraction
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())
        return this as T


    }

    fun enterTextIntoView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())
        return this as T
    }

    fun clearTextOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.clearText())
        return this as T
    }

    fun verifyTextOnView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).check(ViewAssertions.matches(ViewMatchers.withText(text)))

        return this as T
        //Each method returns it's own Class object so we can chain actions
    }

    companion object {
        //Factory Method
        fun <T : CommonUtils<*>> useFactory(screenUtilsClass: Class<T>?): T {
            if (screenUtilsClass == null)
                throw IllegalArgumentException("Pointing to NULL Reference")
            try {
                return screenUtilsClass.newInstance()
            } catch (iae: IllegalStateException) {
                throw RuntimeException("IllegalStateException", iae)
            }
        }
    }
}