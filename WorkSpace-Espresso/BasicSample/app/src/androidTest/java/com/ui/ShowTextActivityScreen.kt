package com.ui

import com.example.android.testing.espresso.BasicSample.R

class ShowTextActivityScreen : CommonUtils<ShowTextActivityScreen>() {


    fun verifyTextOnShowActivity(text: String): ShowTextActivityScreen {
        return verifyTextOnView(EXPTEXT_LABEL, text)
    }

    companion object {


        val EXPTEXT_LABEL = R.id.show_text_view
    }
}