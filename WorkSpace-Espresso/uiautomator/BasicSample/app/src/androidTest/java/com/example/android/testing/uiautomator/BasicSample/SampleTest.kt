package com.example.android.testing.uiautomator.BasicSample

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject2
import androidx.test.uiautomator.Until
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class SampleTest {
    private var mDevice: UiDevice? = null



    @Before
    fun startMainActivityFromHomeScreen() {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice!!.pressHome()

        val launcherPackage = launcherPackageName
        Assert.assertThat(launcherPackage, CoreMatchers.notNullValue())
        mDevice!!.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT.toLong())

       val context = ApplicationProvider.getApplicationContext<Context>()
        val intent = context.packageManager
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE)
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)

        mDevice!!.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)), LAUNCH_TIMEOUT.toLong())
    }
    @Test
    fun checkPreconditions( ){
        Assert.assertThat(mDevice, CoreMatchers.notNullValue())
    }
    @Test
    fun testChangeText_sameActivity() {
        mDevice!!.findObject(By.res(BASIC_SAMPLE_PACKAGE, "editTextUserInput")).text = STRING_TO_BE_TYPED
        mDevice!!.findObject(By.res(BASIC_SAMPLE_PACKAGE, "changeTextBt"))
                .click()

        val changedText = mDevice!!.wait(Until.findObject(By.res(BASIC_SAMPLE_PACKAGE, "textToBeChanged")), 500 )
        Assert.assertThat(changedText.text, CoreMatchers.`is`(CoreMatchers.equalTo(STRING_TO_BE_TYPED)))
    }
    @Test
    fun testChangeText_newActivity() {
        mDevice!!.findObject(By.res(BASIC_SAMPLE_PACKAGE, "editTextUserInput")).text = STRING_TO_BE_TYPED
        mDevice!!.findObject(By.res(BASIC_SAMPLE_PACKAGE, "activityChangeTextBtn"))
                .click()

        val changedText = mDevice!!
                .wait(Until.findObject(By.res(BASIC_SAMPLE_PACKAGE, "show_text_view")),
                        500)
        Assert.assertThat(changedText.text, CoreMatchers.`is`(CoreMatchers.equalTo(STRING_TO_BE_TYPED)))
    }

    private val launcherPackageName: String
         get() {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            val pm = ApplicationProvider.getApplicationContext<Context>().packageManager
            val resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
            return resolveInfo!!.activityInfo.packageName
        }
    companion object {
         val BASIC_SAMPLE_PACKAGE = "com.example.android.testing.uiautomator.BasicSample"
         val LAUNCH_TIMEOUT = 5000
         val STRING_TO_BE_TYPED = "UiAutomator"
    }
}