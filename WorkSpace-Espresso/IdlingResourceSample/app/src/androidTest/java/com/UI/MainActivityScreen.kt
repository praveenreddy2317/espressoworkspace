package com.UI

import com.example.android.testing.espresso.IdlingResourceSample.R

class MainActivityScreen : ScreenUtils<MainActivityScreen>() {

    fun changeText_sameActivity(text:String): MainActivityScreen {

        return clearTextOnView(TEXT_INPUT)
                .enterTextIntoView(TEXT_INPUT, text)
                .clickonView(CHANGETEXT_BUTTON)
                .verifyTextOnView(EXPTEXT_LABEL, text)

    }


    fun verifyTextOnMainActivity(text:String): MainActivityScreen {
        return verifyTextOnView(EXPTEXT_LABEL, text)

    }

    companion object {

        val TEXT_INPUT = R.id.editTextUserInput
        val  CHANGETEXT_BUTTON = R.id.changeTextBt
        val EXPTEXT_LABEL = R.id.textToBeChanged
    }
}