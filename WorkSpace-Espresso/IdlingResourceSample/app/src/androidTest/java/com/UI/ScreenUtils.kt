package com.UI

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
//import com.example.android.testing.espresso.IdlingResourceSample.ChangeTextBehaviorTest
import com.example.android.testing.espresso.IdlingResourceSample.R

@Suppress("UNCHECKED_CAST")
abstract class ScreenUtils<T : ScreenUtils<T>> {

    fun clickonView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())
        return this as T

    }

    fun enterTextIntoView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())
        return this as T
    }

    fun clearTextOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.clearText())
        return this as T
    }
    fun verifyTextOnView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).check(ViewAssertions.matches(ViewMatchers.withText(text)))
        return this as T
    }
    companion object {
        fun <T : ScreenUtils<*>> useFactory(screenUtilsClass: Class<T>?): T {
            if (screenUtilsClass == null)
                throw IllegalArgumentException("Pointing to NULL Reference")
            try {
                return screenUtilsClass.newInstance()
            } catch (iae: IllegalStateException) {
                throw RuntimeException("IllegalStateException", iae)
            }
        }
    }
}