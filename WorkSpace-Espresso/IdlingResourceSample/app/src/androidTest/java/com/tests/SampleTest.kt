package com.tests

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.UI.MainActivityScreen
import com.UI.ScreenUtils.Companion.useFactory
import com.example.android.testing.espresso.IdlingResourceSample.IdlingResource.SimpleIdlingResource
import com.example.android.testing.espresso.IdlingResourceSample.MainActivity
//import com.example.android.testing.espresso.IdlingResourceSample.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SampleTest {
    @get:Rule
    var mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
            MainActivity::class.java)

    //So We have to register Idling resources in test environment.
    @Before
    fun registerResource() {
        IdlingRegistry.getInstance().register(mActivityRule.activity.idlingResource)

    }

    @After
    fun unRegisterResource() {
       IdlingRegistry.getInstance().unregister(mActivityRule.activity.idlingResource)

    }


    @Test
    fun testSynchronizationWithBackGroundJobs() {

        useFactory(MainActivityScreen::class.java).changeText_sameActivity(STRING_TO_BE_TYPED)
                .verifyTextOnMainActivity(EXPECTED_TEXT)

    }


    companion object {

        val STRING_TO_BE_TYPED = "Praveen Kumar"
        val EXPECTED_TEXT = "Praveen Kumar"

    }
}