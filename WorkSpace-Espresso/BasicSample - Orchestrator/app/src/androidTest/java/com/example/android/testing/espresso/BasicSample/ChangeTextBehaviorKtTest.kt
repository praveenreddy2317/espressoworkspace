/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.testing.espresso.BasicSample

import androidx.test.ext.junit.rules.activityScenarioRule
import android.app.Activity
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.filters.RequiresDevice
import com.UI.MainActivityScreen
import com.UI.ScreenUtils
import com.UI.ScreenUtils.Companion.useFactory
import com.UI.ShowTextActivityScreen
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith



@RunWith(AndroidJUnit4::class)
@LargeTest
//@RequiresDevice
class ChangeTextBehaviorKtTest {


    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_changeText_sameActivity() {

        useFactory(MainActivityScreen::class.java).changeText_sameActivity(STRING_TO_BE_TYPED)
                .verifyTextOnMainActivity(EXPECTED_TEXT)


    }

    @Test
    fun test_changeText_newActivity() {

        System.exit(0)
        ScreenUtils.useFactory(MainActivityScreen::class.java)
                .changeText_newActivity(STRING_TO_BE_TYPED)
        ScreenUtils.useFactory(ShowTextActivityScreen::class.java)
                .verifyTextOnShowActivity(EXPECTED_TEXT)

        //Though one test method failed or app is crashed it proceed to next test method and continued the execution,if we use Android Test Orchestrator tool
        // Android Test Orchestrator allows you to run each of your app's tests within its own invocation of Instrumentation.
    }


    companion object {

        val STRING_TO_BE_TYPED = "Espresso"
        val EXPECTED_TEXT = "Espresso"

    }
}