package com.UI

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.example.android.testing.espresso.BasicSample.ChangeTextBehaviorKtTest
import com.example.android.testing.espresso.BasicSample.R

class ShowTextActivityScreen : ScreenUtils<ShowTextActivityScreen>() {


    fun verifyTextOnShowActivity(text:String): ShowTextActivityScreen {
        return verifyTextOnView(EXPTEXT_LABEL,text)
    }

    companion object {


        val EXPTEXT_LABEL = R.id.show_text_view
    }
}