package com.UI

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.example.android.testing.espresso.BasicSample.ChangeTextBehaviorKtTest
import com.example.android.testing.espresso.BasicSample.R

class MainActivityScreen : ScreenUtils<MainActivityScreen>() {

    fun changeText_sameActivity(text:String): MainActivityScreen {

       return clearTextOnView(TEXT_INPUT)
                .enterTextIntoView(TEXT_INPUT, text)
                .clickOnView(CHANGETEXT_BUTTON)
                .verifyTextOnView(EXPTEXT_LABEL, text)

    }

    fun changeText_newActivity(text:String): MainActivityScreen {
        // Type text and then press the button.
       return clearTextOnView(TEXT_INPUT)
               .enterTextIntoView(TEXT_INPUT, text)
               .clickOnView(ACTIVITYCHANGETEXT_BUTTON)

    }
    fun verifyTextOnMainActivity(text:String): MainActivityScreen {
       return verifyTextOnView(EXPTEXT_LABEL, text)

    }

    companion object {

        val TEXT_INPUT = R.id.editTextUserInput
        val  CHANGETEXT_BUTTON = R.id.changeTextBt
        val EXPTEXT_LABEL = R.id.textToBeChanged
        val  ACTIVITYCHANGETEXT_BUTTON = R.id.activityChangeTextBtn
    }
}