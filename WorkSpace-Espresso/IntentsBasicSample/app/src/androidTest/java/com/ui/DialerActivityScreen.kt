package com.ui

import com.example.android.testing.espresso.IntentsBasicSample.R


class DialerActivityScreen : ScreenUtils<DialerActivityScreen>() {

    fun dialANumber(text:String): DialerActivityScreen {

       return clearTextOnView(TEXT_INPUT)
                .enterTextIntoView(TEXT_INPUT, text)
                .clickOnView(CALLNUMBER_BUTTON)


    }



    companion object {
        //NativeElements & IDs
        val TEXT_INPUT = R.id.edit_text_caller_number
        val  CALLNUMBER_BUTTON = R.id.button_call_number
        val  PICKNUMBER_BUTTON = R.id.button_pick_contact

    }
}