import android.app.Activity
import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.android.testing.espresso.IntentsBasicSample.DialerActivity
import com.ui.DialerActivityScreen
import com.ui.ScreenUtils.Companion.useFactory
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class IntentTest {
    @get:Rule
    val activityRule = IntentsTestRule(DialerActivity::class.java)


    @Test
    fun testDialerIntent() {
    //Intended : To verify correct Intent is invoked or not
    //And to verify attributes of Intents

        useFactory(DialerActivityScreen::class.java).dialANumber(PHONE_NUMBER)
        intended(allOf(
                hasAction(Intent.ACTION_CALL), toPackage("com.android.server.telecom")))
    }


    companion object {

        val PHONE_NUMBER = "7897897897"
        val EXPECTED_TEXT = "Espresso"

    }
}