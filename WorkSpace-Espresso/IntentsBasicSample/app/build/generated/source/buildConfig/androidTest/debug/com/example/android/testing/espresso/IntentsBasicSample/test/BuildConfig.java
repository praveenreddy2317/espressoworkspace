/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.android.testing.espresso.IntentsBasicSample.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.android.testing.espresso.IntentsBasicSample.test";
  public static final String BUILD_TYPE = "debug";
}
