package com.example.android.testing.espresso.multiprocesssample

import android.util.Log
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SampleTest {



    @Before
    fun launchActivity() {
        ActivityScenario.launch<DefaultProcessActivity>(DefaultProcessActivity::class.java)
    }

    @Test
    fun verifyAssertingOnViewInRemoteProcessIsSuccessful() {

        Espresso.onView(withId(R.id.textNamedProcess)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.`is`(SampleTest.DEFAULT_PROC_NAME))))
        Log.d(SampleTest.TAG, "Starting activity in a secondary process...")
        Espresso.onView(withId(R.id.startActivityBtn)).perform(ViewActions.click())
        Espresso.onView(withId(R.id.textPrivateProcessName))
                .check(ViewAssertions.matches(ViewMatchers.withText(Matchers.`is`(SampleTest.DEFAULT_PROC_NAME + ":PID2"))))
        Espresso.onData(Matchers.allOf(Matchers.instanceOf<Any>(String::class.java), Matchers.`is`<String>("Doppio"))).perform(ViewActions.click())
        Espresso.onView(withId(R.id.selectedListItemText)).check(ViewAssertions.matches(ViewMatchers.withText("Selected: Doppio")))
    }

    companion object {

        val TAG = "SampleTest"
        val DEFAULT_PROC_NAME = "com.example.android.testing.espresso.multiprocesssample"

    }
}