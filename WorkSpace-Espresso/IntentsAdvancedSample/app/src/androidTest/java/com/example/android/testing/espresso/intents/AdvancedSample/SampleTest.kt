package com.example.android.testing.espresso.intents.AdvancedSample

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SampleTest {


    @get:Rule
    var mIntentsRule = IntentsTestRule(
            ImageViewerActivity::class.java)

    @Before
    fun stubCameraIntent() {
        //Created Stub Intent
        //Intending: To create stub and provide default values to our application
        val result: ActivityResult = createImageCaptureActivityResultStub()
        Intents.intending(IntentMatchers.hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(result)
    }

    @Test
    fun testPhotodrawable() {
        Espresso.onView(withId(R.id.imageView)).check(ViewAssertions.matches(Matchers.not(ImageViewHasDrawableMatcher.hasDrawable())))

        Espresso.onView(withId(R.id.button_take_photo)).perform(ViewActions.click())

        Espresso.onView(withId(R.id.imageView)).check(ViewAssertions.matches(ImageViewHasDrawableMatcher.hasDrawable()))
    }


    private fun createImageCaptureActivityResultStub(): ActivityResult {

        val bundle = Bundle()
        bundle.putParcelable(ImageViewerActivity.KEY_IMAGE_DATA, BitmapFactory.decodeResource(
                mIntentsRule.getActivity().getResources(), R.drawable.ic_launcher))

        val resultData = Intent()
        resultData.putExtras(bundle)

        return ActivityResult(Activity.RESULT_OK, resultData)
    }
}