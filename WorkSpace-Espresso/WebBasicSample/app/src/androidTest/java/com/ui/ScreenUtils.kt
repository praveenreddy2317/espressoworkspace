package com.ui

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.web.assertion.WebViewAssertions.webMatches
import androidx.test.espresso.web.sugar.Web.onWebView
import androidx.test.espresso.web.webdriver.DriverAtoms
import androidx.test.espresso.web.webdriver.DriverAtoms.*
import androidx.test.espresso.web.webdriver.Locator
import org.hamcrest.Matchers.containsString

@Suppress("UNCHECKED_CAST")
abstract class ScreenUtils<T : ScreenUtils<T>> {

    //START:*************-NativeView Actions- ***********************************************
    fun clickOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.click())
        return this as T

    }

    fun enterTextIntoView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())
        return this as T
    }

    fun clearTextOnView(@IdRes viewId: Int): T {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(ViewActions.clearText())
        return this as T
    }
    fun verifyTextOnView(@IdRes viewId: Int, text: String): T {
        Espresso.onView(ViewMatchers.withId(viewId)).check(ViewAssertions.matches(ViewMatchers.withText(text)))
        return this as T
    }
    //END:*************-NativeView Actions- ***********************************************


    //START:*************-WEB View Actions-************************************************

    fun clickOnWebView(webElementId: String): T {
        onWebView().withElement(findElement(Locator.ID, webElementId)).perform(webClick())
        return this as T

    }
    fun enterTextIntoWebView(webElementId: String, text: String): T {
        onWebView().withElement(findElement(Locator.ID, webElementId)).perform(clearElement()).perform(DriverAtoms.webKeys(text))

        return this as T
    }

    fun clearTextOnWebView(webElementId: String): T {
        onWebView().withElement(findElement(Locator.ID, webElementId)).perform(clearElement())
        return this as T
    }
    fun verifyTextOnWebView(webElementId: String, text: String): T {
        onWebView().withElement(findElement(Locator.ID, webElementId)).check(webMatches(getText(), containsString(text)))
        return this as T
    }

    //END:*************-WEB View Actions-************************************************

    companion object {
        fun <T : ScreenUtils<*>> useFactory(screenUtilsClass: Class<T>?): T {
            if (screenUtilsClass == null)
                throw IllegalArgumentException("Pointing to NULL Reference")
            try {
                return screenUtilsClass.newInstance()
            } catch (iae: IllegalStateException) {
                throw RuntimeException("IllegalStateException", iae)
            }
        }
    }
}