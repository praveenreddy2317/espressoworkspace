package com.ui

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers


class WebViewActivityScreen : ScreenUtils<WebViewActivityScreen>() {

    fun changeTextOnSameScreen(text:String): WebViewActivityScreen {

       return clearTextOnWebView(TEXT_INPUT)
                .enterTextIntoWebView(TEXT_INPUT, text)
                .clickOnWebView(CHANGETEXT_BUTTON)
                .verifyTextOnWebView(EXPTEXT_LABEL, text)

    }

    fun changeTextOnNewScreen(text:String): WebViewActivityScreen {

       return clearTextOnWebView(TEXT_INPUT)
               .enterTextIntoWebView(TEXT_INPUT, text)
               .clickOnWebView(SUBMIT_BUTTON)

    }
    fun verifyTextOnHomePage(text:String): WebViewActivityScreen {
       return verifyTextOnWebView(EXPTEXT_LABEL, text)

    }

    companion object {
        //WebElements & IDs
        val TEXT_INPUT = "text_input"
        val  CHANGETEXT_BUTTON = "changeTextBtn"
        val EXPTEXT_LABEL = "message"
        val  SUBMIT_BUTTON = "submitBtn"
    }
}