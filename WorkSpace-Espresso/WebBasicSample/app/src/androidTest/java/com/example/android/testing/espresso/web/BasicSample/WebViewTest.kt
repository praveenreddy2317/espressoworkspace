package com.example.android.testing.espresso.web.BasicSample

import android.app.Activity
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.ui.ScreenUtils.Companion.useFactory
import com.ui.WebViewActivityScreen
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class WebViewTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(WebViewActivity::class.java)

    @Test
    fun test_changeText_sameActivity() {

        //Static Import
        useFactory(WebViewActivityScreen::class.java).changeTextOnSameScreen(STRING_TO_BE_TYPED)
                .verifyTextOnHomePage(EXPECTED_TEXT)

    }




    companion object {

        val STRING_TO_BE_TYPED = "Espresso"
        val EXPECTED_TEXT = "Espresso"

    }
}